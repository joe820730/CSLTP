#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define ROW 9
#define COL 9
#define CR 11
#define CC 11

#ifdef DEBUG
#define PAUSE printf("Press Enter key to continue...\n"); fgetc(stdin);
#endif

int main(int argc, char *argv[])
{
  int i,j,k,l,x,y,counter2;
  int mask_csltp[4];
  int mask_temp[4];
  int threshold;
  
  if(argc>=2)
  {
    threshold=atoi(argv[1]);
  }
  else
  {
	threshold=50;
  }  
  unsigned char srcImg[ROW][COL]=
  {{117,100, 79, 66, 53, 45, 44, 49, 75},
   {112,101, 80, 66, 52, 42, 47, 54, 72},
   {109, 91, 89, 70, 50, 45, 44, 62, 73},
   { 98, 87, 77, 65, 54, 41, 42, 57, 65},
   { 94, 85, 72, 64, 55, 46, 43, 55, 75},
   { 86, 71, 66, 59, 54, 42, 38, 54, 73},
   { 81, 71, 67, 60, 47, 41, 38, 51, 71},
   { 82, 74, 68, 53, 49, 41, 45, 54, 69},
   { 75, 68, 65, 53, 46, 43, 53, 54, 66}
  };//V97:AD105
/*  {{117,100, 79, 66, 53, 45, 44, 49, 75},
   {112,101, 80, 66, 52, 42, 47, 54, 72},
   {109, 91, 89, 70, 50, 45, 44, 62, 73},
   { 98, 87, 77, 65, 54, 41, 42, 57, 65},
   { 94, 85, 72, 64, 55, 46, 43, 55, 75},
   { 86, 71, 66, 59, 54, 42, 38, 54, 73},
   { 81, 71, 67, 60, 47, 41, 38, 51, 71},
   { 82, 74, 68, 53, 49, 41, 45, 54, 69},
   { 75, 68, 65, 53, 46, 43, 53, 54, 66}
  };//V97:AD105 */
  FILE *output;
  unsigned char temp_image[CR][CC]={{0}};
  unsigned char csltp_data[ROW][COL]={{0}};
  printf("Threshold:%d\n",threshold);

//clean the temp space
  for(i=0; i<CR; i++)
  {
    for(j=0; j<CC; j++)
    {
      temp_image[i][j]=0;
    }
  }

//copy image to temp space
  for(k=1; k<CR-1; k++)
  {
    for(l=1; l<CC-1; l++)
    {
      temp_image[k][l]=srcImg[k-1][l-1];
    }
  }


//CSLTP calculating
 for( x = 1 ; x < CR-1; x++)
  {
    for( y = 1; y < CC-1; y++ )
    {
      // 3x3 mask do convolution in here
      int temp = 0;
      mask_csltp[0] = temp_image[x][y+1] - temp_image[x][y-1];
      mask_csltp[1] = temp_image[x+1][y+1] - temp_image[x-1][y-1];
      mask_csltp[2] = temp_image[x+1][y] - temp_image[x-1][y];
      mask_csltp[3] = temp_image[x+1][y-1] - temp_image[x-1][y+1];
      for(counter2 = 0; counter2 < 4; counter2++)
      {
        if(mask_csltp[counter2] >= threshold)
          mask_temp[counter2] = 2;
        else if( abs(mask_csltp[counter2]) < threshold)
          mask_temp[counter2] = 1;
        else if(mask_csltp[counter2] <= -threshold)
          mask_temp[counter2] = 0;
      }
      for(counter2= 0; counter2 < 4; counter2++)
      {
        temp += mask_temp[counter2] * pow(3,counter2);
      }
      csltp_data[x-1][y-1] = temp;
      printf("mask:\n%3d, %3d, %3d\n",temp_image[x-1][y-1],temp_image[x-1][y],temp_image[x-1][y+1]);
      printf("%3d, %3d, %3d\n",temp_image[x][y-1],temp_image[x][y],temp_image[x][y+1]);
      printf("%3d, %3d, %3d\n\n",temp_image[x+1][y-1],temp_image[x+1][y],temp_image[x+1][y+1]);
      printf("CSLTP code:\n%3d, %3d, %3d, %3d\n",mask_csltp[0],mask_csltp[1],mask_csltp[2],mask_csltp[3]);
      printf("%3d, %3d, %3d, %3d\n\n",mask_temp[0],mask_temp[1],mask_temp[2],mask_temp[3]);
      printf("CSLTP value: %3d\n",temp);
      fgetc(stdin);
    }
  }



//Output file
 output=fopen("temp.csv","wb");
  for(i=0;i<9;i++)
  {
    for(j=0;j<9;j++)
    {
      fprintf(output,"%d,",csltp_data[i][j]);
    }
    fprintf(output,"\n");
  }
  fclose(output);
  return 0;
}
