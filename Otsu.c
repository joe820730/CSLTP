#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int Otsu(unsigned char *pixel_1, int width, int height, long filesize)
{
	int pixel, graylevel, graylevel_a;
	int threshold;
	double p1, p2, u1, u2, max_ostu = 0, ostu_temp;
	//**************************************************************************
	int *histogram = malloc(256 * sizeof(int));
	for(graylevel = 0; graylevel < 256; graylevel++)
     histogram[graylevel] = 0;
	//**************************************************************************
	double *histogram_p = malloc(256 * sizeof(double));
	for(graylevel = 0; graylevel < 256; graylevel++)
     histogram_p[graylevel] = 0;
	//**************************************************************************

	for(pixel = 54; pixel < filesize; pixel += 3)
	{
		for(graylevel = 0; graylevel < 256; graylevel++)
		{
			if(pixel_1[pixel] == graylevel)
				histogram[graylevel] ++;
		}
	}

	for(graylevel = 0; graylevel < 256; graylevel++)
	{
		histogram_p[graylevel] = (double)histogram[graylevel] / (width*height);
	}

	for(graylevel = 0; graylevel < 256; graylevel++)
	{
		//****************************************************************************p1 & p2
		p1 = p2 = 0;
		for(graylevel_a = 0; graylevel_a < graylevel+1; graylevel_a++)
		{
			p1 += histogram_p[graylevel_a];
		}
		p2 = 1 - p1;
		//****************************************************************************u1
		u1 = u2 = 0;
		for(graylevel_a = 0; graylevel_a < graylevel+1; graylevel_a++)
		{
			u1 += (graylevel_a * histogram_p[graylevel_a]);
		}
		u1 = u1 / p1;
		//****************************************************************************u2
		for(graylevel_a = graylevel+1; graylevel_a < 256; graylevel_a++)
		{
			u2 += (graylevel_a * histogram_p[graylevel_a]);
        }
        u2 = u2 / p2;
		//****************************************************************************
		ostu_temp = p1 * p2 * pow(u1 - u2, 2);

		if(ostu_temp >= max_ostu)
		{
			max_ostu = ostu_temp;
			threshold = graylevel;
		}
	}

	free(histogram);
	free(histogram_p);

	return threshold;
}
