/* CSLTP (Center-Symmertic Local Ternary Patterns)
 * Author: Joe Tsai
 * Project repository: http://140.127.205.188/m1045113/CSLTP
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Otsu.h"

int image_width(unsigned char *buffer);
int image_height(unsigned char *buffer);
void CSLTP(unsigned char *buffer, int width, int height, int threshold);
void Img2Csv(unsigned char *buffer, int filesize);
void CSLTP2Img(unsigned char *buffer, int filesize);

int main(int argc, char *argv[])
{
  FILE *image = fopen("Lena_gray_24.bmp","rb");
  if(image==NULL)
  {
	printf("Image doesn't exist!!\n");
	return -1;
  }
  fseek(image,0,SEEK_END);
  long filesize = ftell(image);
  rewind(image);
  unsigned char *buffer = (unsigned char*)malloc(filesize * sizeof(unsigned char));
  fread(buffer, filesize, 1, image);
  fclose(image);
  //**********************Get image width and height
  int width = image_width(buffer);
  int height = image_height(buffer);
  int thres = Otsu(buffer, width, height, filesize);
  int threshold = (thres/2 + 0.5);
  if(argc>=2)
  {
	threshold=atoi(argv[1]);
  }

#ifdef EXPORT_ORIGIN_IMG
  FILE *origImg = fopen("origin_img.csv", "wb");
  int pixel;
  int tmp=1;
  for(pixel = 54; pixel < (int)filesize; pixel+=3)
  {
    fprintf(origImg, "%d,", buffer[pixel]);
    if( tmp % width == 0 )
    {
      fprintf(origImg,"\n");
    }
    tmp++;
  }
  fclose(origImg);
#endif
  //*************************image feature(Just needed revise this function)
  CSLTP(buffer, width, height, threshold);
  //*************************Save to *.csv file
  Img2Csv(buffer, (int)filesize);
  CSLTP2Img(buffer, (int)filesize);

  free(buffer);

  return 0;
}

int image_width(unsigned char *buffer)
{
  int width = (long)buffer[18] +
      ((long)buffer[19] << 8) +
      ((long)buffer[20] << 16) +
      ((long)buffer[21] << 24);

  return width;
}

int image_height(unsigned char *buffer)
{
  int height = (long)buffer[22] +
      ((long)buffer[23] << 8) +
      ((long)buffer[24] << 16) +
      ((long)buffer[25] << 24);

  return height;
}

void CSLTP(unsigned char *buffer, int width, int height, int threshold)
{
  int x, y;
  int mask_csltp[4];
  int mask_temp[4];
  int counter1 = 0, counter2 = -1, counter3 = 0;

  /* Create a temp 2-D array (column-Pointer) to calculate CSLTP */
  unsigned char **temp_image = (unsigned char**)calloc((height + 2) , sizeof(unsigned char*));
  /* Create a temp 2-D array (each-row-data) to calculate CSLTP */
  for(x = 0; x < height+2; x++)
    temp_image[x] = (unsigned char*)calloc((width + 2) , sizeof(unsigned char));


  /* Create a 2-D array to store each CSLTP 3x3 mask result */
  unsigned char **csltp_data= (unsigned char**)calloc(height , sizeof(unsigned char*));
  for(x = 0; x < height; x++)
    csltp_data[x] = (unsigned char*)calloc(width , sizeof(unsigned char));

  /* Import image to temp array */
  for(x = 1; x < height+1; x++)
  {
    for(y = 1; y < width+1; y++)
    {
      counter1 += 1;
      temp_image[x][y] = buffer[54+(counter1-1)*3];
    }
  }

/* CSLTP calculating area, still working. */
  for( x = 1 ; x < height+1; x++)
  {
    for( y = 1; y < width+1; y++ )
    {
      // 3x3 mask do convolution in here
      int temp = 0;
      mask_csltp[0] = temp_image[x][y+1] - temp_image[x][y-1];
      mask_csltp[1] = temp_image[x+1][y+1] - temp_image[x-1][y-1];
      mask_csltp[2] = temp_image[x+1][y] - temp_image[x-1][y];
      mask_csltp[3] = temp_image[x+1][y-1] - temp_image[x-1][y+1];
      for(counter2 = 0; counter2 < 4; counter2++)
      {
        if(mask_csltp[counter2] >= threshold)
          mask_temp[counter2] = 2;
        else if( abs(mask_csltp[counter2]) < threshold)
          mask_temp[counter2] = 1;
        else if(mask_csltp[counter2] <= -threshold)
          mask_temp[counter2] = 0;
      }
      for(counter2= 0; counter2 < 4; counter2++)
      {
        temp += mask_temp[counter2] * pow(3,counter2);
      }
      csltp_data[x-1][y-1] = temp;
    }
  }

  //After CSLTP calculate then write to original input data
  for(x = 0; x < height; x++)
  {
    for(y = 0; y < width; y++)
    {
      counter3 += 1;

      buffer[54+(counter3-1)*3] = csltp_data[x][y];
      buffer[55+(counter3-1)*3] = csltp_data[x][y];
      buffer[56+(counter3-1)*3] = csltp_data[x][y];
    }
  }

  for(x = 0; x < height+2; x++)
    free(temp_image[x]);
  for(x = 0; x < height; x++)
    free(csltp_data[x]);

  free(temp_image);
  free(csltp_data);
}

void Img2Csv(unsigned char *buffer, int filesize)
{
  FILE *CSLTPcsv = fopen("CSLTP.csv", "wb");
  fprintf(CSLTPcsv, "CSLTP_mode1\n");
  int pixel;
  for(pixel = 54; pixel < (int)filesize; pixel+=3)
  {
    fprintf(CSLTPcsv, "%lf\n", (float)buffer[pixel]);
  }
  fclose(CSLTPcsv);
}

void CSLTP2Img(unsigned char *buffer, int filesize)
{
  int i;
  for(i=54;i<(int)filesize; i++)
  {
	buffer[i]=buffer[i]*3;
  }
  FILE *img = fopen("csltp.bmp", "wb");
  fwrite(buffer, filesize,1,img);
  fclose(img);
}
