all: CSLTP OTSU
	gcc Otsu.o CSLTP.o -o test.exe -lm -Wall

CSLTP: CSLTP.c 
	gcc -c CSLTP.c -o CSLTP.o -lm -Wall

OTSU: Otsu.c Otsu.h
	gcc -c Otsu.c -o Otsu.o -lm -Wall

debug: OTSU
	gcc -c CSLTP.c -o CSLTP.o -lm -D DEBUG
	gcc Otsu.o CSLTP.o -o test.exe -lm -Wall -D DEBUG

exportres: OTSU
	gcc -c CSLTP.c -o CSLTP.o -lm -D EXPORT_ORIGIN_IMG 
	gcc Otsu.o CSLTP.o -o test.exe -lm -Wall

check: CSLTP_check.c
	gcc -o check.exe CSLTP_check.c -lm -Wall

.PHONY: clean
clean:
	rm *.o
	rm *.exe

.PHONY: test
test: all
	./test.exe

.PHONY: report
report: check
	./check.exe
